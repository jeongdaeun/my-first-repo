def print_even_backward(word1, word2):
	words = [word1, word2,]

	for i in range(1,5+1):
		if i%2==0:
	  		print(' '.join(words[::-1]))
		else:
    	  		print(' '.join(words))

if __name__ == '__main__':
	input1 = input('first word > ')
	input2 = input('last word > ')
	print_even_backward(input1, input2)
